# Mancala API

Game development project using SpringBoot Web and Swagger2. Mancala rule can be found in wikipedia: https://en.wikipedia.org/wiki/Mancala.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites

What things you need to install the software and how to install them

* Apache Maven 3.3.9
+ Java 1.8.0_91

## Running the tests


```
mvn clean install
```

## Runnnig the SpringBoot


```
mvn spring-boot:run
```