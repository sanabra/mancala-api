package com.example.mancala;

import java.util.Arrays;

import org.springframework.stereotype.Service;

@Service
public class MancalaService {

	public Board newGame(String playerA, String playerB) {
		return new Board(playerA, playerB);
	}

	public Resume execute(Movement moviment) {
		if (moviment.getPosition() < 0 || moviment.getPosition() >= Board.DEFAULT_MAX_HOLLOWS) {
			throw new IllegalArgumentException(String.format("Position value is not valid. Position value (0-%s)",
					(Board.DEFAULT_MAX_HOLLOWS - 1)));
		}
		int stones = getMySide(moviment.getTurn(), moviment.getBoard()).getHollows()[moviment.getPosition()];
		getMySide(moviment.getTurn(), moviment.getBoard()).getHollows()[moviment.getPosition()] = 0;
		if (stones == 0) {
			throw new IllegalArgumentException(
					String.format("Position not contains stones. Please select another position (0-%s)",
							(Board.DEFAULT_MAX_HOLLOWS - 1)));
		}

		Resume resume = shareStone(//
				moviment.getPosition() + 1, //
				stones, //
				moviment.getTurn(), //
				moviment.getBoard());

		return resume;
	}

	private Turn oppositePlayer(Turn turn) {
		return Turn.A.equals(turn) ? Turn.B : Turn.A;
	}

	private boolean noStonesToPlay(Board board) {
		int[] hollowsEmpty = Side.initializeHollows(board.getSideA().getHollows().length, 0);
		return Arrays.equals(board.getSideA().getHollows(), hollowsEmpty) || //
				Arrays.equals(board.getSideB().getHollows(), hollowsEmpty);
	}

	private int collectStones(Turn turn, Board board, int myLastPosition) {
		int oppositePosition = Math.abs(myLastPosition - Board.DEFAULT_MAX_HOLLOWS) - 1;
		int stonesToBeCollected = getOppositeHollows(turn, board)[oppositePosition];
		getOppositeHollows(turn, board)[oppositePosition] = 0;
		getMySide(turn, board).setMancala(getMySide(turn, board).getMancala() + stonesToBeCollected);
		return stonesToBeCollected;
	}

	private Resume shareStone(int position, int stones, Turn turn, Board board) {

		if (stones > 0) {
			int nextPosition = 0;

			if (isOppositeSide(position)) { // opponent positions

				int opponentPosition = position % (Board.DEFAULT_MAX_HOLLOWS + 1);
				getOppositeHollows(turn, board)[opponentPosition] = //
						getOppositeHollows(turn, board)[opponentPosition++] + 1;

				if (opponentPosition == Board.DEFAULT_MAX_HOLLOWS) {
					nextPosition = 0;
				} else {
					nextPosition = Board.DEFAULT_MAX_HOLLOWS + 1 + opponentPosition;
				}

			} else if (isMyMancalaPosition(position)) {

				getMySide(turn, board).setMancala(getMySide(turn, board).getMancala() + 1);

				if (lastStone(stones)) {
					return new Resume(board, Status.EXTRA_MOVEMENT, turn,
							String.format("Player %s won extra movement!", turn));
				}

				nextPosition = Board.DEFAULT_MAX_HOLLOWS + 1;

			} else { // my positions

				getMyHollows(turn, board)[position] = //
						getMyHollows(turn, board)[position] + 1;

				if (lastStone(stones) && getMyHollows(turn, board)[position] == 1) {
					int collectedStones = collectStones(turn, board, position);

					if (noStonesToPlay(board)) {
						String additionalInfo = "";
						int i = whosWon(board);
						if (i > 0) {
							additionalInfo += String.format("%s WON! Congrats!", board.getPlayerA());
						} else if (i < 0) {
							additionalInfo += String.format("%s WON! Congrats!", board.getPlayerB());
						} else {
							additionalInfo += "Draw!";
						}

						return new Resume(board, Status.END_GAME, null,
								additionalInfo + String.format(" (Player A %sx%s Player B)",
										board.getSideA().getMancala(), board.getSideB().getMancala()));
					} else {
						return new Resume(board, Status.COLLECT_STONES, oppositePlayer(turn),
								String.format("Was collected %s stones.", collectedStones));
					}
				} else {
					nextPosition = position + 1;
				}
			}

			return shareStone(nextPosition, --stones, turn, board);
		}
		return new Resume(board, Status.OK, oppositePlayer(turn));
	}

	private int whosWon(Board board) {
		int mancalaA = board.getSideA().getMancala();
		int mancalaB = board.getSideB().getMancala();
		return Integer.compare(mancalaA, mancalaB);
	}

	private boolean isOppositeSide(int position) {
		return position > Board.DEFAULT_MAX_HOLLOWS;
	}

	private boolean isMyMancalaPosition(int position) {
		return position == Board.DEFAULT_MAX_HOLLOWS;
	}

	private boolean lastStone(int stones) {
		return stones == 1;
	}

	private Side getMySide(Turn turn, Board board) {
		if (Turn.A.equals(turn)) {
			return board.getSideA();
		} else {
			return board.getSideB();
		}
	}

	private int[] getMyHollows(Turn turn, Board board) {
		if (Turn.A.equals(turn)) {
			return board.getSideA().getHollows();
		} else {
			return board.getSideB().getHollows();
		}
	}

	private int[] getOppositeHollows(Turn turn, Board board) {
		if (Turn.A.equals(turn)) {
			return board.getSideB().getHollows();
		} else {
			return board.getSideA().getHollows();
		}
	}

}
