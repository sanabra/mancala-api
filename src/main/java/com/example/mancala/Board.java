package com.example.mancala;

import javax.validation.constraints.NotNull;

public class Board {

	static final int DEFAULT_MAX_HOLLOWS = 6;
	static final int DEFAULT_STARTER_STONES_HOLLOW = 4;

	private String playerA;
	private String playerB;

	@NotNull
	private Side sideA;

	@NotNull
	private Side sideB;

	public Board() {
		this("Player A", "Player B");
	}

	public Board(String playerA, String playerB) {
		this.playerA = playerA;
		this.playerB = playerB;
		sideA = new Side(DEFAULT_MAX_HOLLOWS, DEFAULT_STARTER_STONES_HOLLOW);
		sideB = new Side(DEFAULT_MAX_HOLLOWS, DEFAULT_STARTER_STONES_HOLLOW);
	}

	public String getPlayerA() {
		return playerA;
	}

	public void setPlayerA(String playerA) {
		this.playerA = playerA;
	}

	public String getPlayerB() {
		return playerB;
	}

	public void setPlayerB(String playerB) {
		this.playerB = playerB;
	}

	public Side getSideA() {
		return sideA;
	}

	public void setSideA(Side sideA) {
		this.sideA = sideA;
	}

	public Side getSideB() {
		return sideB;
	}

	public void setSideB(Side sideB) {
		this.sideB = sideB;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Board [");
		if (sideA != null) {
			builder.append("A=");
			builder.append(sideA.toString());
			builder.append(", ");
		}
		if (sideB != null) {
			builder.append("B=");
			builder.append(sideB.toString());
		}
		builder.append("]");
		return builder.toString();
	}

}
