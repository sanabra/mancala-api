package com.example.mancala;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mancala")
public class MancalaRestController {

	@Autowired
	private MancalaService mancalaService;

	@GetMapping
	Board newGame(//
			@RequestParam(defaultValue = "Player A") String playerA, //
			@RequestParam(defaultValue = "Player B") String playerB) {
		return mancalaService.newGame(playerA, playerB);
	}

	@PostMapping("/play")
	Resume play(@RequestBody(required = true) Movement moviment) {
		return mancalaService.execute(moviment);
	}

}
