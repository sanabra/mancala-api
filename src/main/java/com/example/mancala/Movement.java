package com.example.mancala;

import javax.validation.constraints.NotNull;

public class Movement {

	@NotNull
	private int position;

	@NotNull
	private Turn turn;

	@NotNull
	private Board board;

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public Turn getTurn() {
		return turn;
	}

	public void setTurn(Turn turn) {
		this.turn = turn;
	}

}
