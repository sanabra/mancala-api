package com.example.mancala;

public class Resume {

	private Board board;
	private Turn nextTurn;
	private Status status;
	private String additionalInfo;

	public Resume(Board board, Status status, Turn nextTurn) {
		this.setBoard(board);
		this.setStatus(status);
		this.setNextTurn(nextTurn);
	}

	public Resume(Board board, Status status, Turn nextTurn, String additionalInfo) {
		this.setBoard(board);
		this.setStatus(status);
		this.setNextTurn(nextTurn);
		this.setAdditionalInfo(additionalInfo);
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Turn getNextTurn() {
		return nextTurn;
	}

	public void setNextTurn(Turn nextTurn) {
		this.nextTurn = nextTurn;
	}

}
