package com.example.mancala;

import java.util.Arrays;

import javax.validation.constraints.NotNull;

public class Side {

	@NotNull
	private int[] hollows;

	@NotNull
	private int mancala;

	public Side() {

	}

	public Side(int max_hollows, int starter_stones_hollow) {
		hollows = initializeHollows(max_hollows, starter_stones_hollow);
		mancala = 0;
	}

	public int[] getHollows() {
		return hollows;
	}

	public void setHollows(int[] hollows) {
		this.hollows = hollows;
	}

	public int getMancala() {
		return mancala;
	}

	public void setMancala(int mancala) {
		this.mancala = mancala;
	}

	static public int[] initializeHollows(int max_hollows, int starter_stones_hollow) {
		int[] hollows = new int[max_hollows];
		for (int i = 0; i < hollows.length; i++) {
			hollows[i] = starter_stones_hollow;
		}
		return hollows;
	}

	@Override
	public String toString() {
		return "[hollows=" + Arrays.toString(hollows) + ", mancala=" + mancala + "]";
	}

}