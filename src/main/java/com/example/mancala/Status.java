package com.example.mancala;

public enum Status {

	OK, EXTRA_MOVEMENT, COLLECT_STONES, END_GAME

}
