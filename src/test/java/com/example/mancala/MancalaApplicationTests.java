package com.example.mancala;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MancalaApplicationTests {

	@Autowired
	private MancalaService mancalaService;

	@Test
	public void contextLoads() {
		assertNotNull(mancalaService);
	}

	@Test
	public void startNewGameDefaultTest() {
		Board result = mancalaService.newGame("Player A", "Player B");
		assertBoard(result, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0);
	}

	@Test
	public void playScenarioExtraMovementTurnATest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.A);
		moviment.setPosition(2);
		moviment.setBoard(createBoard(//
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.EXTRA_MOVEMENT, result.getStatus());
		assertEquals("Next turn", Turn.A, result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 4, 4, 0, 5, 5, 5 }, 1, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0);
	}

	@Test
	public void playScenarioExtraMovementTurnBTest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.B);
		moviment.setPosition(2);
		moviment.setBoard(createBoard(//
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.EXTRA_MOVEMENT, result.getStatus());
		assertEquals("Next turn", Turn.B, result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 4, 0, 5, 5, 5 }, 1);
	}

	@Test
	public void playScenario2TurnATest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.A);
		moviment.setPosition(1);
		moviment.setBoard(createBoard(//
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.OK, result.getStatus());
		assertEquals("Next turn", Turn.B, result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 4, 0, 5, 5, 5, 5 }, 0, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0);
	}

	@Test
	public void playScenario2TurnBTest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.B);
		moviment.setPosition(1);
		moviment.setBoard(createBoard(//
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.OK, result.getStatus());
		assertEquals("Next turn", Turn.A, result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 4, 0, 5, 5, 5, 5 }, 0);
	}

	@Test
	public void playScenarioCollectStonesTurnATest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.A);
		moviment.setPosition(5);
		moviment.setBoard(createBoard(//
				new int[] { 1, 0, 6, 6, 6, 9 }, 1, //
				new int[] { 2, 3, 4, 5, 6, 7 }, 8));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.COLLECT_STONES, result.getStatus());
		assertEquals("AdditionalInfo", "Was collected 7 stones.", result.getAdditionalInfo());
		assertEquals("Next turn", Turn.B, result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 1 + 1, 0 + 1, 6, 6, 6, 0 }, 1 + 1 + 6 + 1, //
				new int[] { 2 + 1, 3 + 1, 4 + 1, 5 + 1, 0, 7 + 1 }, 8);
	}

	@Test
	public void playScenarioEndGameTurnATest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.A);
		moviment.setPosition(2);
		moviment.setBoard(createBoard(//
				new int[] { 0, 0, 1, 0, 0, 8 }, 10, //
				new int[] { 0, 0, 1, 0, 0, 0 }, 10));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.END_GAME, result.getStatus());
		assertEquals("AdditionalInfo", "Player A WON! Congrats! (Player A 11x10 Player B)", result.getAdditionalInfo());
		assertNull("Next turn", result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 0, 0, 0, 1, 0, 8 }, 11, //
				new int[] { 0, 0, 0, 0, 0, 0 }, 10);
	}

	@Test
	public void playScenarioEndGameTurnBTest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.B);
		moviment.setPosition(2);
		moviment.setBoard(createBoard(//
				new int[] { 0, 0, 1, 0, 0, 0 }, 10, //
				new int[] { 0, 0, 1, 0, 0, 8 }, 10));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.END_GAME, result.getStatus());
		assertEquals("AdditionalInfo", "Player B WON! Congrats! (Player A 10x11 Player B)", result.getAdditionalInfo());
		assertNull("Next turn", result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 0, 0, 0, 0, 0, 0 }, 10, //
				new int[] { 0, 0, 0, 1, 0, 8 }, 11);
	}

	@Test
	public void playScenarioEndGameDrawTest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.B);
		moviment.setPosition(2);
		moviment.setBoard(createBoard(//
				new int[] { 0, 0, 1, 0, 0, 0 }, 11, //
				new int[] { 0, 0, 1, 0, 0, 8 }, 10));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.END_GAME, result.getStatus());
		assertEquals("AdditionalInfo", "Draw! (Player A 11x11 Player B)", result.getAdditionalInfo());
		assertNull("Next turn", result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 0, 0, 0, 0, 0, 0 }, 11, //
				new int[] { 0, 0, 0, 1, 0, 8 }, 11);
	}

	@Test
	public void playScenarioCollectStonesTurnBTest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.B);
		moviment.setPosition(5);
		moviment.setBoard(createBoard(//
				new int[] { 2, 3, 4, 5, 6, 7 }, 8, //
				new int[] { 9, 0, 6, 6, 6, 9 }, 1));
		Resume result = mancalaService.execute(moviment);

		assertEquals("Status", Status.COLLECT_STONES, result.getStatus());
		assertEquals("AdditionalInfo", "Was collected 7 stones.", result.getAdditionalInfo());
		assertEquals("Next turn", Turn.A, result.getNextTurn());
		assertBoard(result.getBoard(), //
				new int[] { 2 + 1, 3 + 1, 4 + 1, 5 + 1, 0, 7 + 1 }, 8, //
				new int[] { 9 + 1, 0 + 1, 6, 6, 6, 9 - 9 }, 1 + 1 + 6 + 1);
	}

	@Test
	public void playScenarioNoStonesTurnATest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.A);
		moviment.setPosition(1);
		moviment.setBoard(createBoard(//
				new int[] { 0, 0, 6, 6, 6, 10 }, 1, //
				new int[] { 4, 4, 4, 4, 4, 4 }, 0));

		try {
			mancalaService.execute(moviment);
			fail();
		} catch (Exception e) {
			assertEquals("Position not contains stones. Please select another position (0-5)", e.getMessage());
		}
	}

	@Test
	public void playScenarioNoStonesTurnBTest() {
		Movement moviment = new Movement();
		moviment.setTurn(Turn.B);
		moviment.setPosition(1);
		moviment.setBoard(createBoard(//
				new int[] { 4, 4, 4, 4, 4, 4 }, 0, //
				new int[] { 0, 0, 6, 6, 6, 10 }, 1));

		try {
			mancalaService.execute(moviment);
			fail();
		} catch (Exception e) {
			assertEquals("Position not contains stones. Please select another position (0-5)", e.getMessage());
		}
	}

	private Board createBoard(int[] hollowsA, int mancalaA, int[] hollowsB, int mancalaB) {
		Board board = mancalaService.newGame("Player A", "Player B");

		board.getSideA().setHollows(hollowsA);
		board.getSideA().setMancala(mancalaA);

		board.getSideB().setHollows(hollowsB);
		board.getSideB().setMancala(mancalaB);

		return board;
	}

	static private void assertBoard(Board board, int[] hollowsA, int mancalaA, int[] hollowsB, int mancalaB) {
		assertNotNull(board);

		assertNotNull("Board:playerA", board.getPlayerA());
		assertNotNull("Board:playerB", board.getPlayerB());

		assertEquals("Board:playerA", "Player A", board.getPlayerA());
		assertEquals("Board:playerB", "Player B", board.getPlayerB());

		assertNotNull("Board:sideA", board.getSideA());
		assertNotNull("Board:sideB", board.getSideB());

		assertEquals("Board:mancalaA " + board.toString(), mancalaA, board.getSideA().getMancala());
		assertEquals("Board:mancalaB " + board.toString(), mancalaB, board.getSideB().getMancala());

		assertArrayEquals("Board:hollowsA " + board.toString(), hollowsA, board.getSideA().getHollows());
		assertArrayEquals("Board:hollowsB " + board.toString(), hollowsB, board.getSideB().getHollows());

	}

}
